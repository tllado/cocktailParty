The Cocktail Party Problem can be solved in just a few lines of code, as seen here.

[https://raw.githubusercontent.com/tllado/cocktailParty/master/hw2ICAShort.m](https://raw.githubusercontent.com/tllado/cocktailParty/master/hw2ICAShort.m)

![alt text](https://raw.githubusercontent.com/tllado/cocktailParty/master/results.png)

