D = open('sounds.mat');         % Open source data file
S = D.sounds(1:5, 10001:10500); % Extract source signal data
X = rand(5)*S;                  % Randomly combine source signals to create microphone signals
W = rand(5, 5)*2/5;             % Select random values for initial reconstructor
for n = 1:1e7                   % Iterate toward optimal reconstruction
    Y = W*X;                                % Create current reconstruction
    Z = 1./(1 + exp(-Y));                   % Compare current reconstruction to expected distribution
    dW = 1e-3*(eye(5) + (1 - 2*Z)*(Y)')*W;  % Calculate gradient toward expected distribution
    W = W + dW;                             % Move toward expected distribution
end
for n = 1:5 % Plot originals, reconstructions, and microphones
    subplot(5, 3, 3*n - 2); plot(S(n, :), 'r'); xlabel('Original')
    subplot(5, 3, 3*n - 1); plot(Y(n, :), 'b'); xlabel('Reconstruction')
    subplot(5, 3, 3*n); plot(X(n, :), 'g'); xlabel('Microphone')
end
